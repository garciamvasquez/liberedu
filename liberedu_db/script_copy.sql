-- Crear la base de datos
CREATE DATABASE liberedu;

-- Usar la base de datos
\ c liberedu 

-- Create parents table
CREATE TABLE parents (
  id SERIAL PRIMARY KEY,
  first_name VARCHAR(100) NOT NULL,
  surename VARCHAR(100) NOT NULL,
  id_card INT,
  email VARCHAR(100) NOT NULL,
  rol VARCHAR(8)
);

-- Create teachers table
CREATE TABLE teachers (
  id SERIAL PRIMARY KEY,
  first_name VARCHAR(100) NOT NULL,
  surename VARCHAR(100) NOT NULL,
  id_card INT,
  email VARCHAR(100) UNIQUE NOT NULL,
  contrasena VARCHAR(20) NOT NULL,
  rol VARCHAR(8)
);

-- Create groups table
CREATE TABLE groups (
  id SERIAL PRIMARY KEY,
  teacher_id SERIAL REFERENCES teachers(id),
  group_name VARCHAR(10) NOT NULL,
);

-- Create students table
CREATE TABLE students (
  id SERIAL PRIMARY KEY,
  parent_id SERIAL REFERENCES parents(id),
  group_id SERIAL REFERENCES groups(id),
  first_name VARCHAR(100) NOT NULL,
  surename VARCHAR(100) NOT NULL,
  id_card INT,
  email VARCHAR(100) UNIQUE NOT NULL,
  contrasena VARCHAR(100) NOT NULL,,
  rol VARCHAR(8)
);

-- Create course table
CREATE TABLE courses (
  id SERIAL PRIMARY KEY,
  teacher_id SERIAL REFERENCES teachers(id),
  course_name VARCHAR(100) NOT NULL,
  descripcion TEXT
);

-- Create students table
CREATE TABLE groups_courses (
  group_id SERIAL REFERENCES groups(id),
  courses_id SERIAL REFERENCES courses(id)
);

-- Create grade table
CREATE TABLE grades (
  id SERIAL PRIMARY KEY,
  student_id SERIAL REFERENCES students(id),
  course_id SERIAL REFERENCES courses(id),
  grade NUMERIC(3, 2),
  grade_percentage INT
);

-- Create admins table
CREATE TABLE admins (
  id SERIAL PRIMARY KEY,
  first_name VARCHAR(100) NOT NULL,
  surename VARCHAR(100) NOT NULL,
  id_card INT,
  email VARCHAR(100) UNIQUE NOT NULL,
  contrasena VARCHAR(20) NOT NULL,
  rol VARCHAR(8)
);