-- Crear la base de datos
CREATE DATABASE liberedu;

-- Create public."admin" table
CREATE TABLE public."admin" (
  idCard INT PRIMARY KEY,
  first_name VARCHAR(100) NOT NULL,
  surename VARCHAR(100) NOT NULL,
  email VARCHAR(100) UNIQUE NOT NULL,
  user_password VARCHAR(20) NOT NULL,
  user_role VARCHAR(8) NOT NULL CHECK (
    user_role IN (
      'admin',
      'teacher',
      'student'
    )
  )
);
-- Create parent table
CREATE TABLE parent (
  id SERIAL PRIMARY KEY,
  first_name VARCHAR(100) NOT NULL,
  surename VARCHAR(100) NOT NULL,
  id_card INT,
  email VARCHAR(100) NOT NULL,
  user_role VARCHAR(8) NOT NULL CHECK (
    user_role IN (
      'admin',
      'teacher',
      'student'
    )
  )
);

-- Create teacher table
CREATE TABLE teacher (
  id SERIAL PRIMARY KEY,
  first_name VARCHAR(100) NOT NULL,
  surename VARCHAR(100) NOT NULL,
  id_card INT,
  email VARCHAR(100) UNIQUE NOT NULL,
  user_password VARCHAR(20) NOT NULL,
  user_role VARCHAR(8) NOT NULL CHECK (
    user_role IN (
      'admin',
      'teacher',
      'student'
    )
  )
);

-- Create group table
CREATE TABLE "group" (
  id SERIAL PRIMARY KEY,
  teacher_id SERIAL,
  group_name VARCHAR(10) NOT NULL,
  CONSTRAINT teacker_fk 
    FOREIGN KEY(teacher_id) 
      REFERENCES teacher(id)
      ON DELETE CASCADE 
      ON UPDATE CASCADE
);

-- Create student table
CREATE TABLE student (
  id SERIAL PRIMARY KEY,
  parent_id SERIAL,
  group_id SERIAL,
  first_name VARCHAR(100) NOT NULL,
  surename VARCHAR(100) NOT NULL,
  id_card INT,
  email VARCHAR(100) UNIQUE NOT NULL,
  user_password VARCHAR(100) NOT NULL,
  user_role VARCHAR(8) NOT NULL CHECK (
    user_role IN (
      'admin',
      'teacher',
      'student'
    )
  ),
  CONSTRAINT parent_fk FOREIGN KEY(parent_id) REFERENCES parent(id) ON UPDATE  CASCADE,
  CONSTRAINT group_fk FOREIGN KEY(group_id) REFERENCES group(id) ON UPDATE CASCADE
);

-- Create course table
CREATE TABLE course (
  id SERIAL PRIMARY KEY,
  teacher_id SERIAL,
  course_name VARCHAR(100) NOT NULL,
  "description" TEXT,
  CONSTRAINT teacher_fk FOREIGN KEY(teacher_id) REFERENCES teacher(id) ON UPDATE CASCADE
);

-- Create student table
CREATE TABLE group_course (
  group_id SERIAL,
  course_id SERIAL,
  CONSTRAINT group_fk FOREIGN KEY(group_id) REFERENCES group(id) ON UPDATE CASCADE,
  CONSTRAINT course_fk FOREIGN KEY(course_id) REFERENCES course(id) ON UPDATE CASCADE
);

-- Create grade table
CREATE TABLE grade (
  id SERIAL PRIMARY KEY,
  student_id SERIAL,
  course_id SERIAL,
  grade NUMERIC(3, 2) NOT NULL,
  grade_percentage INT NOT NULL,
  CONSTRAINT student_fk FOREIGN KEY(student_id) REFERENCES student(id) ON UPDATE CASCADE,
  CONSTRAINT course_fk FOREIGN KEY(course_id) REFERENCES course(id) ON UPDATE CASCADE
);